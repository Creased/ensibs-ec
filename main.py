#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""TP Elliptic Curve."""

# Generate documentation: epydoc -v --html main.py -o ./docs
# Make tests: python3 -m doctest -v main.py

import collections
import secrets
import hashlib
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '30 October 2017'

Coord = collections.namedtuple('Coord', ['x', 'y'])

def egcd(a_value, b_value):
    """Extended great common divisor.

    >>> egcd(23, 7)
    (1, -3, 10)
    >>> egcd(2, 9)
    (1, -4, 1)
    >>> egcd(0.2, 9)
    Traceback (most recent call last):
        ...
    TypeError: a must be a valid integer!

    @param a_value: First number.
    @type a_value: C{int}
    @raise a_value: TypeError, a must be a valid integer (C{int}).

    @param b_value: Second number.
    @type b_value: C{int}
    @raise b_value: TypeError, b must be a valid integer (C{int}).

    @return: gcd(a,b), u, v so au + bv = gcd(a,b)
    """
    if not isinstance(a_value, int):
        raise TypeError('a must be a valid integer!')
    elif not isinstance(b_value, int):
        raise TypeError('b must be a valid integer!')
    elif a_value == 0:
        return (b_value, 0, 1)
    else:
        g_value, y_value, x_value = egcd(b_value % a_value, a_value)
        return (g_value, x_value - (b_value//a_value) * y_value, y_value)

def modular_inverse(number, modulus):
    """Compute the modular multiplicative inverse of an integer within modulus.

    >>> modular_inverse(1, 13)
    1
    >>> modular_inverse(3, 26)
    9
    >>> modular_inverse(5, 89)
    18
    >>> modular_inverse(10, 64)
    Traceback (most recent call last):
        ...
    ValueError: Modular inverse of 10 modulo 64 doesn't exist!

    @param number: Number to search for modular multiplicative inverse.
    @type number: C{int}
    @raise number: TypeError, number must be a valid integer (C{int}).

    @param modulus: Modulus.
    @type modulus: C{int}
    @raise modulus: TypeError, modulus must be a valid integer (C{int}).

    @raise modulus: ValueError, modular inverse doesn't exist

    @return: modular multiplicative inverse of specified number (C{int}).
    """
    g_value, u_value, _ = egcd(number, modulus)
    if g_value != 1:
        raise ValueError('Modular inverse of {} modulo {} doesn\'t exist!'.format(number, modulus))
    else:
        return u_value % modulus

class EllipticCurve(object):
    """Elliptic Curve utils."""
    def __init__(self, a_coef, b_coef, point, p_field):
        """Constructor of I{EllipticCurve}.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> (ec.a_coef, ec.b_coef, ec.point, ec.p_field)
        (1, 3, Coord(x=3, y=4), 17)
        >>> ec = EllipticCurve(1, 3, Coord(3, 5), 17)
        Traceback (most recent call last):
            ...
        ValueError: p point must be on the elliptic curve!

        @param self: Current instance of I{EllipticCurve}.
        @type self: C{EllipticCurve}

        @param a_coef: a coefficient of elliptic curve in affine restricted Weierstrass form.
        @type a_coef: C{int}
        @raise a_coef: TypeError, a must be a valid integer (C{int}).

        @param b_coef: b coefficient of elliptic curve in affine restricted Weierstrass form.
        @type b_coef: C{int}
        @raise b_coef: TypeError, b must be a valid integer (C{int}).

        @param point: point on the elliptic curve.
        @type point: C{Coord}
        @raise point: TypeError, point must be a valid named tuple (C{Coord}).
        @raise point: ValueError, point must be on the elliptic curve.

        @param p_field: field size.
        @type p_field: C{int}
        @raise p_field: TypeError, b must be a valid integer (C{int}).
        """
        if not isinstance(a_coef, int):
            raise TypeError('min must be a valid integer!')
        elif not isinstance(b_coef, int):
            raise TypeError('max must be a valid integer!')
        elif not isinstance(point, Coord):
            raise TypeError('point must be a valid named tuple!')
        elif not isinstance(p_field, int):
            raise TypeError('p field must be a valid integer!')
        else:
            self.a_coef = a_coef
            self.b_coef = b_coef
            self.p_field = p_field
            self.identity = Coord(None, None)
            self.p_order = 0

            if self.check_point(point):
                self.point = point
            else:
                raise ValueError('p point must be on the elliptic curve!')

    def check_point(self, point):
        """Check if point is on the elliptic curve.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.check_point(Coord(6, 2))
        True
        >>> ec.check_point(Coord(7, 6))
        False
        >>> ec.check_point(Coord(7, 9))
        True

        @param point: point on the elliptic curve.
        @type point: C{Coord}
        @raise point: TypeError, p point must be a valid named tuple (C{Coord}).
        """
        if not isinstance(point, Coord):
            raise TypeError('point must be a valid named tuple!')
        else:
            left_part = (point.y ** 2) % self.p_field
            right_part = ((point.x ** 3) + self.a_coef * point.x + self.b_coef) % self.p_field

            return point != self.identity and left_part == right_part

    def add(self, point_1, point_2):
        """Add two points on elliptic curve.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.add(Coord(6, 2), Coord(2, 8))
        Coord(x=7, y=8)
        >>> ec.add(Coord(6, 2), Coord(6, 2))
        Coord(x=7, y=9)
        >>> ec.add(Coord(6, 2), Coord(6, 4))
        Traceback (most recent call last):
            ...
        ValueError: point 2 must be on the elliptic curve!

        @param point_1: point on the elliptic curve.
        @type point_1: C{Coord}
        @raise point_1: TypeError, point 1 must be a valid named tuple (C{Coord}).
        @raise point_1: ValueError, point 1 must be on the elliptic curve.

        @param point_2: point on the elliptic curve.
        @type point_2: C{Coord}
        @raise point_2: TypeError, point 2 must be a valid named tuple (C{Coord}).
        @raise point_2: ValueError, point 2 must be on the elliptic curve.
        """
        if not isinstance(point_1, Coord):
            raise TypeError('point 1 must be a valid named tuple!')
        elif not isinstance(point_2, Coord):
            raise TypeError('point 2 must be a valid named tuple!')
        else:
            if point_1 == self.identity:
                result = point_2
            elif point_2 == self.identity:
                result = point_1
            elif not self.check_point(point_1):
                raise ValueError('point 1 must be on the elliptic curve!')
            elif not self.check_point(point_2):
                raise ValueError('point 2 must be on the elliptic curve!')
            elif point_1.x == point_2.x and point_1.y != point_2.y:
                result = self.identity
            else:
                if point_1.x == point_2.x and point_1.y == point_2.y:  # 2P
                    lambda_ = (3 * (point_1.x * point_1.x) + self.a_coef) \
                              * modular_inverse((2 * point_1.y) \
                              % self.p_field, self.p_field) % self.p_field
                    x_value = (lambda_ * lambda_ - (2 * point_1.x)) % self.p_field
                    y_value = (lambda_ * (point_1.x - x_value) - point_1.y) % self.p_field
                else:  # P+Q
                    lambda_ = (point_2.y - point_1.y) \
                              * modular_inverse((point_2.x - point_1.x) \
                              % self.p_field, self.p_field) % self.p_field
                    x_value = (lambda_ * lambda_ - point_1.x - point_2.x) % self.p_field
                    y_value = (lambda_ * (point_1.x - x_value) - point_1.y) % self.p_field

                result = Coord(x_value, y_value)

            return result

    def neg(self, point):
        """Negate point on elliptic curve.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.neg(Coord(6, 2))
        Coord(x=6, y=15)
        >>> ec.neg(Coord(7, 9))
        Coord(x=7, y=8)
        >>> ec.neg(Coord(6, 4))
        Traceback (most recent call last):
            ...
        ValueError: point must be on the elliptic curve!

        @param point: point on the elliptic curve.
        @type point: C{Coord}
        @raise point: TypeError, point must be a valid named tuple (C{Coord}).
        """
        if self.check_point(point):
            return Coord(point.x, -point.y % self.p_field)
        else:
            raise ValueError('point must be on the elliptic curve!')

    def sub(self, point_1, point_2):
        """Substract two points on elliptic curve.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.sub(Coord(7, 8), Coord(2, 8))
        Coord(x=6, y=2)
        >>> ec.sub(Coord(6, 4), Coord(2, 8))
        Traceback (most recent call last):
            ...
        ValueError: point 1 must be on the elliptic curve!

        @param point_1: point on the elliptic curve.
        @type point_1: C{Coord}
        @raise point_1: TypeError, point 1 must be a valid named tuple (C{Coord}).
        @raise point_1: ValueError, point 1 must be on the elliptic curve.

        @param point_2: point on the elliptic curve.
        @type point_2: C{Coord}
        @raise point_2: TypeError, point 2 must be a valid named tuple (C{Coord}).
        @raise point_2: ValueError, point 2 must be on the elliptic curve.
        """
        if not isinstance(point_1, Coord):
            raise TypeError('point 1 must be a valid named tuple!')
        elif not isinstance(point_2, Coord):
            raise TypeError('point 2 must be a valid named tuple!')
        elif not self.check_point(point_1):
            raise ValueError('point 1 must be on the elliptic curve!')
        elif not self.check_point(point_2):
            raise ValueError('point 2 must be on the elliptic curve!')
        else:
            result = self.add(point_1, self.neg(point_2))
            return result

    def mul(self, n_count, point):
        """n times multiplication of elliptic curve point.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.mul(4, Coord(2, 8))
        Coord(x=8, y=8)
        >>> ec.mul(17, Coord(2, 8))
        Coord(x=None, y=None)
        >>> ec.mul(4, Coord(6, 4))
        Traceback (most recent call last):
            ...
        ValueError: point must be on the elliptic curve!

        @param n_count: n count for nP multiplication.
        @type n_count: C{int}
        @raise n_count: TypeError, n must be a valid integer (C{int}).

        @param point: point on the elliptic curve.
        @type point: C{Coord}
        @raise point: TypeError, point must be a valid named tuple (C{Coord}).
        @raise point: ValueError, point must be on the elliptic curve.
        """
        if not isinstance(n_count, int):
            raise TypeError('point 1 must be a valid named tuple!')
        elif not isinstance(point, Coord):
            raise TypeError('point must be a valid named tuple!')
        elif not self.check_point(point):
            raise ValueError('point must be on the elliptic curve!')
        else:
            res = self.identity
            tmp_point = point
            while n_count > 0:
                if n_count & 1 == 1:
                    res = self.add(res, tmp_point)
                n_count = n_count >> 1
                tmp_point = self.add(tmp_point, tmp_point)
            return res

    def __add__(self, point_2):
        """Overload + operator."""
        return self.add(self.point, point_2)

    def __iadd__(self, point_2):
        """Overload += operator."""
        self = self + point_2
        return self

    def __neg__(self):
        """Overload the - unary operator."""
        return self.neg(self.point)

    def __sub__(self, point_2):
        """Overload the - binary operator."""
        return self.sub(self.point, point_2)

    def __isub__(self, point_2): # Overload the "-=" operator
        """Overload the -= operator."""
        self = self - point_2
        return self

    def set_order(self, order):
        """Define point order.

        @param order: point order on the elliptic curve.
        @type order: C{int}
        @raise order: TypeError, point order must be a valid integer (C{int}).
        """
        if not isinstance(order, int):
            raise TypeError('point order must be a valid integer!')
        else:
            self.p_order = order

    def order(self, point):
        """Return order of elliptic curve point.

        >>> ec = EllipticCurve(1, 3, Coord(3, 4), 17)
        >>> ec.order(Coord(2, 8))
        17
        >>> ec.order(Coord(6, 4))
        Traceback (most recent call last):
            ...
        ValueError: point must be on the elliptic curve!

        @param point: point on the elliptic curve.
        @type point: C{Coord}
        @raise point: TypeError, point must be a valid named tuple (C{Coord}).
        @raise point: ValueError, point must be on the elliptic curve.
        """
        if not isinstance(point, Coord):
            raise TypeError('point must be a valid named tuple!')
        elif not self.check_point(point):
            raise ValueError('point must be on the elliptic curve!')
        else:
            i = 1
            while self.mul(i, point) != self.identity and i <= self.p_field:
                i += 1
            return i

class ElGamal(object):
    """ElGamal encryption on elliptic curve."""
    def __init__(self, elliptic_curve):
        """Constructor of I{ElGamal}.

        @param self: Current instance of I{ElGamal}.
        @type self: C{ElGamal}

        @param elliptic_curve: Elliptic curve.
        @type elliptic_curve: C{EllipticCurve}
        @raise elliptic_curve: TypeError, a must be a valid elliptic curve (C{EllipticCurve}).
        """
        if not isinstance(elliptic_curve, EllipticCurve):
            raise TypeError('elliptic_curve must be a valid elliptic curve!')
        else:
            self.elliptic_curve = elliptic_curve
            self.point = elliptic_curve.point
            self.cardinal = elliptic_curve.p_field

    def encrypt(self, plain, public_key, rand):
        """Encrypt message.

        @param plain: Plain data on the elliptic curve.
        @type plain: C{Coord}
        @raise plain: TypeError, plain must be a valid named tuple (C{Coord}).
        @raise plain: ValueError, plain must be on the elliptic curve.

        @param public_key: Public key on the elliptic curve.
        @type public_key: C{Coord}
        @raise public_key: TypeError, public key must be a valid named tuple (C{Coord}).
        @raise public_key: ValueError, public key must be on the elliptic curve.

        @param rand: Random integer.
        @type rand: C{int}
        @raise rand: TypeError, Random must be a valid integer (C{int}).
        """
        if not isinstance(plain, Coord):
            raise TypeError('plain must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(plain):
            raise ValueError('plain must be on the elliptic curve!')
        elif not isinstance(public_key, Coord):
            raise TypeError('public key must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(public_key):
            raise ValueError('public key must be on the elliptic curve!')
        elif not isinstance(rand, int):
            raise TypeError('rand must be a valid integer!')
        else:
            cipher_part1, cipher_part2 = (self.elliptic_curve.mul(rand, self.point),
                                          self.elliptic_curve.add(plain, \
                                          self.elliptic_curve.mul(rand, public_key)))
            return (cipher_part1, cipher_part2)

    def decrypt(self, cipher, private_key):
        """Decrypt message.

        @param cipher: Cipher data on the elliptic curve.
        @type cipher: C{tuple}
        @raise cipher: TypeError, cipher must be a valid tuple (C{tuple}).
        @raise cipher[0]: TypeError, part 1 of cipher must be a valid named tuple (C{Coord}).
        @raise cipher[1]: TypeError, part 2 of cipher must be a valid named tuple (C{Coord}).
        @raise cipher[0]: ValueError, part 1 of cipher must be on the elliptic curve.
        @raise cipher[1]: ValueError, part 2 of cipher must be on the elliptic curve.

        @param private_key: Private key.
        @type private_key: C{int}
        @raise private_key: TypeError, private key must be a valid integer (C{int}).
        """
        if not isinstance(cipher, tuple):
            raise TypeError('cipher must be a valid tuple!')
        elif not isinstance(cipher[0], Coord):
            raise TypeError('part 1 of cipher must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(cipher[0]):
            raise ValueError('part 1 of cipher must be on the elliptic curve!')
        elif not isinstance(cipher[1], Coord):
            raise TypeError('part 2 of cipher must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(cipher[1]):
            raise ValueError('part 2 of cipher must be on the elliptic curve!')
        elif not isinstance(private_key, int):
            raise TypeError('private key must be a valid integer!')
        else:
            cipher_part1, cipher_part2 = cipher
            return self.elliptic_curve.add(cipher_part2, \
            self.elliptic_curve.neg(self.elliptic_curve.mul(private_key, cipher_part1)))

class ECDSA(object):
    """DSA over Elliptic Curve."""
    def __init__(self, elliptic_curve):
        """Constructor of I{ECDSA}.

        @param self: Current instance of I{ECDSA}.
        @type self: C{ECDSA}

        @param elliptic_curve: Elliptic curve.
        @type elliptic_curve: C{EllipticCurve}
        @raise elliptic_curve: TypeError, a must be a valid elliptic curve (C{EllipticCurve}).
        """
        if not isinstance(elliptic_curve, EllipticCurve):
            raise TypeError('elliptic_curve must be a valid elliptic curve!')
        else:
            self.elliptic_curve = elliptic_curve
            self.hash = hashlib.sha256

    def sign(self, private_key, message):
        """Sign message with ECDSA.

        @param private_key: Private key.
        @type private_key: C{int}
        @raise private_key: TypeError, private key must be a valid integer (C{int}).

        @param message: Message to sign.
        @type message: C{str}
        @raise message: TypeError, Message must be a valid string.
        """
        if not isinstance(private_key, int):
            raise TypeError('private key must be a valid integer!')
        elif not isinstance(message, str):
            raise TypeError('message must be a valid string!')
        else:
            u_value = 0
            v_value = 0
            while u_value == 0 or v_value == 0:
                rand = (1 + secrets.randbelow(self.elliptic_curve.p_order - 1))
                k_point = self.elliptic_curve.mul(rand, self.elliptic_curve.point)
                u_value = k_point.x % self.elliptic_curve.p_order
                hashed_message = int(self.hash(message.encode()).hexdigest(), 16)
                v_value = (modular_inverse(rand, self.elliptic_curve.p_order) \
                          * (hashed_message + u_value * private_key)) % self.elliptic_curve.p_order

            return (u_value, v_value)

    def verify(self, public_key, message, signature):
        """Sign message with ECDSA.

        @param public_key: Public key on the elliptic curve.
        @type public_key: C{Coord}
        @raise public_key: TypeError, public key must be a valid named tuple (C{Coord}).
        @raise public_key: ValueError, public key must be on the elliptic curve.

        @param message: Message to sign.
        @type message: C{str}
        @raise message: TypeError, Message must be a valid string.

        @param signature: signature.
        @type signature: C{tuple}
        @raise signature: TypeError, signature must be a valid tuple (C{tuple}).
        """
        if not isinstance(public_key, Coord):
            raise TypeError('public key must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(public_key):
            raise ValueError('public key must be on the elliptic curve!')
        elif not isinstance(message, str):
            raise TypeError('message must be a valid string!')
        elif not isinstance(signature, tuple):
            raise TypeError('signature must be a valid tuple!')
        else:
            (u_value, v_value) = signature
            if (u_value < 1 or u_value > self.elliptic_curve.p_order - 1) \
               or (v_value < 1 or v_value > self.elliptic_curve.p_order - 1):
                result = False
            else:
                hashed_message = int(self.hash(message.encode()).hexdigest(), 16)
                modinv_v = modular_inverse(v_value, self.elliptic_curve.p_order) \
                           % self.elliptic_curve.p_order
                u_part1 = (hashed_message * modinv_v) % self.elliptic_curve.p_order
                u_part2 = (u_value * modinv_v) % self.elliptic_curve.p_order
                xy_point = self.elliptic_curve.add(\
                           self.elliptic_curve.mul(u_part1,
                                                   self.elliptic_curve.point),
                           self.elliptic_curve.mul(u_part2,
                                                   public_key))
                final_v = xy_point.x % self.elliptic_curve.p_order
                result = (u_value == final_v)

            return result

class DiffieHellman(object):
    """Elliptic Curve Diffie Hellman Key Exchange."""
    def __init__(self, elliptic_curve):
        """Constructor of I{DiffieHellman}.

        @param self: Current instance of I{DiffieHellman}.
        @type self: C{DiffieHellman}

        @param elliptic_curve: Elliptic curve.
        @type elliptic_curve: C{EllipticCurve}
        @raise elliptic_curve: TypeError, a must be a valid elliptic curve (C{EllipticCurve}).
        """
        if not isinstance(elliptic_curve, EllipticCurve):
            raise TypeError('elliptic_curve must be a valid elliptic curve!')
        else:
            self.elliptic_curve = elliptic_curve
            self.point = elliptic_curve.point
            self.cardinal = elliptic_curve.p_field

    def gen_public_key(self, private_key):
        """Generate public key.

        @param private_key: Private key.
        @type private_key: C{int}
        @raise private_key: TypeError, private key must be a valid integer (C{int}).
        """
        if not isinstance(private_key, int):
            raise TypeError('private key must be a valid integer!')
        else:
            return self.elliptic_curve.mul(private_key, self.point)

    def secret(self, private_key, public_key):
        """Calculate shared secret for public and private key.

        @param private_key: Private key.
        @type private_key: C{int}
        @raise private_key: TypeError, private key must be a valid integer (C{int}).

        @param public_key: Public key on the elliptic curve.
        @type public_key: C{Coord}
        @raise public_key: TypeError, public key must be a valid named tuple (C{Coord}).
        @raise public_key: ValueError, public key must be on the elliptic curve.
        """
        if not isinstance(public_key, Coord):
            raise TypeError('public key must be a valid named tuple!')
        elif not self.elliptic_curve.check_point(public_key):
            raise ValueError('public key must be on the elliptic curve!')
        elif not isinstance(private_key, int):
            raise TypeError('private key must be a valid integer!')
        elif self.elliptic_curve.mul(self.cardinal, public_key) == self.elliptic_curve.identity:
            raise TypeError('invalid private key!')
        else:
            return self.elliptic_curve.mul(private_key, public_key)

def create_elliptice_curve():
    """Create FRP256v1 EC. More info: https://go.bmoine.fr/vqYiLK7wqGiWZXfj"""
    a_coef = 0xF1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C00
    b_coef = 0xEE353FCA5428A9300D4ABA754A44C00FDFEC0C9AE4B1A1803075ED967B7BB73F
    point = Coord(0xB6B3D4C356C139EB31183D4749D423958C27D2DCAF98B70164C97A2DD98F5CFF,
                  0x6142E0F7C8B204911F9271F0F3ECEF8C2701C307E8E4C9E183115A1554062CFB)
    p_field = 0xF1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C03
    p_order = 0xF1FD178C0B3AD58F10126DE8CE42435B53DC67E140D2BF941FFDD459C6D655E1

    elliptic_curve = EllipticCurve(a_coef, b_coef, point, p_field)
    elliptic_curve.set_order(p_order)

    return elliptic_curve

def generate_key_pair(diffie_hellman):
    """Generate key pair for EC."""
    priv = (1 + secrets.randbelow(diffie_hellman.cardinal - 1)) # 1 to el_gamal.cardinal - 1
    pub = diffie_hellman.gen_public_key(priv)

    return (priv, pub)

def main():
    """EllipticCurve main process."""
    try:
        # Create elliptic curve
        elliptic_curve = create_elliptice_curve()
        el_gamal = ElGamal(elliptic_curve)
        diffie_hellman = DiffieHellman(elliptic_curve)
        ecdsa = ECDSA(elliptic_curve)

        log.success('Elliptic Curve:\n')
        log.bold('a: {}'.format(elliptic_curve.a_coef))
        log.bold('b: {}'.format(elliptic_curve.b_coef))
        log.bold('p: {}'.format(elliptic_curve.point))
        log.bold('n: {}\n'.format(elliptic_curve.p_field))

        # Key generation
        # Alice key generation
        alice_priv, alice_pub = generate_key_pair(diffie_hellman)

        # Bob key generation
        bob_priv, bob_pub = generate_key_pair(diffie_hellman)

        log.success('Key generation:\n')
        log.bold('Alice:')
        log.bold('Private Key: {}'.format(alice_priv))
        log.bold('Public Key: {}\n'.format(alice_pub))

        log.bold('Bob:')
        log.bold('Private Key: {}'.format(bob_priv))
        log.bold('Public Key: {}\n'.format(bob_pub))

        # Key exchange
        log.success('Key exchange:\n')
        log.bold('Alice:')
        log.bold('Send: Alice public key')
        log.bold('Receive: Bob public key\n')

        log.bold('Bob:')
        log.bold('Send: Bob public key')
        log.bold('Receive: Alice public key\n')

        # Shared secret calculation
        alice_key = diffie_hellman.secret(alice_priv, bob_pub)
        bob_key = diffie_hellman.secret(bob_priv, alice_pub)

        log.success('Shared secret calculation:\n')
        log.bold('Alice:')
        log.bold('k: {}\n'.format(alice_key))

        log.bold('Bob:')
        log.bold('k: {}\n'.format(bob_key))

        # Alice send a secret message to Bob
        message = Coord(0xda40db8a1ff2c45970740382ae2241d214bfb6d5033ad5f759fc1ea5ac2b9fcc,
                        0x9b4097fe159595af9b57f5b759b60b0c4166370255f76d2bdd2cf314e1ed5531)
        rand = (1 + secrets.randbelow(el_gamal.cardinal - 1)) # 1 to eg.cardinal - 1
        cipher = el_gamal.encrypt(message, bob_pub, rand)

        # Bob receive secret message and decrypt it
        retrieved_message = el_gamal.decrypt(cipher, bob_priv)

        log.success('Encrypted message exchange:\n')
        log.bold('Alice:')
        log.bold('Clear message: {}'.format(message))
        log.bold('Encrypted message: {}\n'.format(cipher))

        log.bold('Bob:')
        log.bold('Encrypted message: {}'.format(cipher))
        log.bold('Decrypted message: {}\n'.format(retrieved_message))

        # Message signature
        message = "Blah"
        signature = ecdsa.sign(alice_priv, message)

        log.success('Message signature:\n')
        log.bold('Alice:')
        log.bold('Message: {}'.format(message))
        log.bold('Signature: {}\n'.format(signature))

        log.bold('Bob:')
        log.bold('Message: {}'.format(message))
        log.bold('Signature: {}'.format(signature))
        log.bold('Verification result: {}'.format(ecdsa.verify(alice_pub, message, signature)))
    except (KeyError, TypeError, ValueError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
